import React, { useState } from 'react';
import "./Chessboard.css";


export default function Chessbord() {
  const [highlightedCells, setHighlightedCells] = useState({});
  const handleCellClick = (e) => {
    const { rowIndex, cellIndex } = e.target.dataset;
    const row = parseInt(rowIndex);
    const col = parseInt(cellIndex);

    const hash = {
      [`${row}-${col}`]: true,
      ...findTopLeft(row, col, {}),
      ...findTopRight(row, col, {}),
      ...findBottomLeft(row, col, {}),
      ...findBottomRight(row, col, {})
    };
    setHighlightedCells(hash);
  };

  const findTopLeft = (row, col, hash) => {
    row--;
    col--;
    while (row >= 0 && col >= 0) {
      hash[`${row}-${col}`] = true;
      row--;
      col--;
    }
    return hash;
  };

  const findTopRight = (row, col, hash) => {
    row--;
    col++;
    while (row >= 0 && col < 8) {
      hash[`${row}-${col}`] = true;
      row--;
      col++;
    }
    return hash;
  };



  const findBottomLeft = (row, col, hash) => {
    row++;
    col--;
    while (row < 8 && col >= 0) {
      hash[`${row}-${col}`] = true;
      row++;
      col--;
    }
    return hash;
  };

  const findBottomRight = (row, col, hash) => {
    row++;
    col++;
    while (row < 8 && col < 8) {
      hash[`${row}-${col}`] = true;
      row++;
      col++;
    }
    return hash;
  };
  const rows = [];
  for (let i = 0; i < 8; i++) {
    const cells = [];
    for (let j = 0; j < 8; j++) {
      const isWhite = (i + j) % 2 === 0;
      const cellKey = `${i}-${j}`;
      cells.push(
        <td
          key={cellKey}
          data-row-index={i}
          data-cell-index={j}
          className={`box ${isWhite ? 'white' : 'black'} ${highlightedCells[cellKey] ? 'red' : ''}`}
          onClick={handleCellClick}
        />
      );
    }
    rows.push(<tr key={i}>{cells}</tr>);
  }
  return rows;
}   