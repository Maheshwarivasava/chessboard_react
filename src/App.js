import './App.css';
import Chessboard from './components/Chessboard.jsx';

function App() {
  return (
    <div>
      <h1>Chessboard</h1>
      <table id="table">
        <tbody>
          <Chessboard/>
        </tbody>
      </table>
    </div>
  );
}

export default App;
